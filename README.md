# Emergency 4 Game Protocol
## Client
| What       | Count | Type                | How    |
|------------|-------|---------------------|--------|
| `43000000` |     1 | Hello               | |
| `0e000000` |  1826 | Ping                | |
| `4c000000` |    33 | Client Chat Message | UTF-16 encoded message prefixed by character length |
| `47000000` |     1 | ? |
| `49000000` |     1 | ? |
| `0a000000` |     1 | ? |
| `0c000000` |   336 | ? |
| `09000000` |   139 | ? |
| `36000000` |    45 | ? |
| `5b000000` |     5 | ? |
| `00000067` |     1 | ? |
| `1fb00700` |     1 | ? |
| `0bb00700` |     1 | ? |

## Server
Might be compressed, the *type* parameter may be compressed as well.    
`<Uint32>PacketID <Uint32>BitLength ???`

| What   | Type |
|--------|------|
| `00e0` | ? |
| `0200` | ? |
| `0300` | ? |
| `030c` | ? |
| `0400` | ? |
| `0500` | ? |
| `0600` | ? |
| `0d00` | ? |
| `1100` | ? |
| `1300` | ? |
| `1400` | ? |
| `1500` | ? |
| `1600` | ? |
| `1800` | ? |
| `1900` | ? |
| `1a00` | ? |
| `1b00` | ? |
| `1c00` | ? |
| `1f00` | ? |
| `2000` | ? |
| `2500` | ? |
| `273c` | ? |
| `3100` | ? |
| `3200` | ? |
| `3500` | ? |
| `3900` | ? |
| `3a00` | ? |
| `3b00` | ? |
| `3f00` | ? |
| `4000` | ? |
| `4100` | ? |
| `4500` | ? |
| `4600` | ? |
| `4800` | ? |
| `4a00` | ? |
| `4b00` | ? |
| `4d00` | ? |
| `4e00` | ? |
| `4f00` | ? |
| `5000` | ? |
| `5300` | ? |
| `5400` | ? |
| `5500` | ? |
| `55e1` | ? |
| `5e00` | ? |
| `5f00` | ? |
| `62fe` | ? |
| `6400` | ? |
| `6de3` | ? |
| `e351` | ? |

### Events
| Events |
|-------------------------|
| MPAbortTFMBUnloadEvent |
| MPAbortTFMBUploadEvent |
| MPAbortTRLUnloadEvent |
| MPAbortTRLUploadEvent |
| MPAckOrderEvent |
| MPAckPingEvent |
| MPAddBloodspotEvent |
| MPAddFirehoseEvent |
| MPAddRadarPingEvent |
| MPAddRifleToGetawayCarEvent |
| MPAddToSelectionHintEvent |
| MPApplyBodyForceEvent |
| MPApplyForceEvent |
| MPAttachSoundEvent |
| MPBuyVehicleEvent |
| MPCameraMovedEvent |
| MPChangeModelAsfEvent |
| MPChangePrototypeEvent |
| MPChangeWeatherEvent |
| MPChatEvent |
| MPChildEnabledEvent |
| MPCommandEvent |
| MPCreateActorEvent |
| MPCreateObjectEvent |
| MPDisablePhysicsEvent |
| MPDisablePhysicsObjectEvent |
| MPDisconnectionReasonEvent |
| MPEnablePhysicsEvent |
| MPEnablePhysicsObjectEvent |
| MPExplodeVehicleEvent |
| MPFinishFreeplayEvent |
| MPFreezePhysicsEvent |
| MPGameMessageEvent |
| MPHelloEvent |
| MPLeaveCarEvent |
| MPLoadMapEvent |
| MPLobbyAnnouncementEvent |
| MPLobbyReadyStateEvent |
| MPLobbyServerNameEvent |
| MPMapLoadedEvent |
| MPMatchFinishedEvent |
| MPNetworkPausedEvent |
| MPNetworkResumedEvent |
| MPObjectDataEvent |
| MPOnPlayerDisconnectedEvent |
| MPOrderEvent |
| MPPauseParticleEffectEvent |
| MPPingEvent |
| MPPlayAnimCloseDoorByNameEvent |
| MPPlayAnimCloseDoorEvent |
| MPPlayAnimOpenDoorByNameEvent |
| MPPlayAnimOpenDoorEvent |
| MPPlayDLKBasketAnimEvent |
| MPPlayRwLightAnimationEvent |
| MPPlaySample2dEvent |
| MPPlaySample3dAndReplaceEvent |
| MPPlaySample3dByRefEvent |
| MPPlaySample3dEvent |
| MPPublishScoreEvent |
| MPRemoveFirehoseEvent |
| MPRemoveFromSelectionEvent |
| MPRemoveObjectEvent |
| MPRemoveParticleEffectEvent |
| MPReportAwardReceptionEvent |
| MPReportAwardsEvent |
| MPReportChatEvent |
| MPReportCommandPosEvent |
| MPReportRadarPingEvent |
| MPResumeParticleEffectEvent |
| MPRunParticleEffectEvent |
| MPSelectionChangedEvent |
| MPSellVehicleEvent |
| MPShowFreeplayEvent |
| MPShowFreeplayEvent || FailedEvent |
| MPStartCamShakeEvent |
| MPStartHeliDustEffectEvent |
| MPStartMatchEvent |
| MPStartParticleEffectEvent |
| MPStartTFMBUnloadEvent |
| MPStartTFMBUploadEvent |
| MPStartTRLUnloadEvent |
| MPStartTRLUploadEvent |
| MPStopParticleEffectEvent |
| MPStopSampleEvent |
| MPSyncEvent |
| MPUnattachSoundEvent |
| MPUnfreezePhysicsEvent |
| MPUpdateSceneTimeEvent |
| MPVehicleBoughtEvent |
| MPVehicleOrderedEvent |
| MPVehicleReturnedEvent |
| MPVehicleSoldEvent |

